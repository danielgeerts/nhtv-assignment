/**
 * Copyright 2016 Daniel Geerts <daniel.geertsu@live.nl>
 *
 * @file Grenade.h
 */

#ifndef GRENADE_H
#define GRENADE_H

// Include NHTVgame files
#include "basicentity.h"
#include "explosion.h"

/// @brief The Grenade class is the Entity implementation.
class Grenade : public BasicEntity
{
public:
	/// @brief Constructor
	Grenade();
	/// @brief Destructor
	virtual ~Grenade();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

private:
	/* add your private declarations */

	/// @brief Add the grenade sprite
	BasicEntity* grenade;
	/// @brief Add the explosion spritesheet
	Explosion* explosion;

	/// @brief Some variables for the explosion
	bool addExplosion = true;
};

#endif /* GRENADE_H */
