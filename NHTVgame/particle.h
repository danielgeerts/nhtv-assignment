/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Particle.h
 */

#ifndef PARTICLE_H
#define PARTICLE_H

#include <rt2d/entity.h>

/// @brief The Particle class is the Entity implementation.
class Particle : public Entity
{
public:
	/// @brief Constructor
	Particle(int bombpart, int grenadepart);
	/// @brief Destructor
	virtual ~Particle();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief Boolean 'trigger' can be used in extended files from Particle files for a trigger
	/// For example: You can acces this trigger from a array with the type Particle and use
	/// this trigger in the extended files, after use set this to false
	bool trigger = false;

	/// @brief Rotate direction, add this every frame to the current Rotation
	float rotateDirection = 0.0f;

	/// @brief Direction this particle goes
	Point2 velocity;

	/// @brief when set true, this particle wil be removed and deleted
	bool needToDestroy = false;

private:
	/// @brief new velocity
	Point2 addableVelocity;

	/// @brief when true, change this particles sprite color
	bool changeColor = false;

	/// @brief calculate new color
	RGBAColor newColor;
	bool toYellow = false;
	bool toGray = false;
	bool red = false;
	bool green = false;

	float newred = newColor.r;
	float newgreen = newColor.g;
};

#endif /* PARTICLE_H */
