/**
 * This class describes ParticleSystem behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "ParticleSystem.h"

ParticleSystem::ParticleSystem(bool isBomb) : Entity()
{
	// ###############################################################
	// Create some particles
	// ###############################################################
	for (int i = 0; i < 25; i++) {
		if (i <= 2) {
			if (isBomb) {
				addNewParticle(i, -1);
			} else {
				addNewParticle(-1, i);
			}
		} else {
			addNewParticle(-1, -1);
		}
	}
}

ParticleSystem::~ParticleSystem()
{
	// deconstruct and delete particles
	for (int i = 0; i < particles.size(); i++) {
		this->removeChild(particles[i]);
		delete particles[i];
		particles[i] = NULL;
	}
	particles.clear();
}

void ParticleSystem::update(float deltaTime)
{
	// ###############################################################
	// Something is wrong in the RT2D framework (i geuss)
	// so the first element has the parents position, scale and rotation
	// but the rest of the elements not, so i give them the worldposition
	// of the particlesystem and adds it to the single particle position
	// ###############################################################
	for (int i = 0; i < particles.size(); i++) {
		if (particles[i] != NULL) {
			//std::cout << "particle: " + std::to_string(i) + " pos: " + std::to_string(particles[i]->position.x) + ", " + std::to_string(particles[i]->position.y) + " worldpos: " + std::to_string(particles[i]->worldpos().x) + ", " + std::to_string(particles[i]->worldpos().y) << std::endl;
			if (i != 0)
				particles[i]->position = this->worldpos() + particles[i]->velocity;
			else if (i == 0)
				particles[i]->position += particles[i]->velocity * deltaTime;

			this->trigger = particles[i]->trigger;
		}
	}

	// ###############################################################
	// Erase all particles that are in a need to be destroyed
	// deconstruct and delete particles
	// ###############################################################
	bool clearParticleList = false;
	for (int i = 0; i < particles.size(); i++) {
		if (particles[i] != NULL) {
			if (particles[i]->needToDestroy) {
				this->removeChild(particles[i]);
				delete particles[i];
				particles[i] = NULL;
				clearParticleList = true;
			}
		}
	}
	// if every particle equals NULL then clear the List
	if (clearParticleList) {
		// Just to be sure, remove some left over particles (Like the parts of the bomb and grenade)
		for (int i = 0; i < particles.size(); i++) {
			if (particles[i] != NULL) {
				this->removeChild(particles[i]);
				delete particles[i];
				particles[i] = NULL;
			}
		}
		particles.clear();
		this->needToDestroySystem = true;
	}
}

// ###############################################################
// add new particle to the system
// bombpart stands for a part of the bomb, in total there are 3 parts (0,1,2)
// grenadepart stands for a part of the grenade, in total there are 3 parts (0,1,2)
// Add the new particle to the particles list and to the stage
// ###############################################################
void ParticleSystem::addNewParticle(int bombpart, int grenadepart) {
	// Create new particle
	Particle* temppart = new Particle(bombpart, grenadepart);

	// Add temppart to the list and add to the particlesystem
	particles.push_back(temppart);
	this->addChild(temppart);
}