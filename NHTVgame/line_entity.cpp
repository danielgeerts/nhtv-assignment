/**
 * This class describes Line_Entity behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "Line_Entity.h"

Line_Entity::Line_Entity() : Entity()
{
	// Define _line
	_line = new Line();
}

Line_Entity::~Line_Entity()
{
}

void Line_Entity::update(float deltaTime)
{
}

// ###############################################################
// Bind all Points
// ###############################################################
void Line_Entity::BindPoints(RGBAColor color, float linewidth) {
	_line->color = color;
	_line->lineWidth = linewidth;
	this->addLine(_line);
}