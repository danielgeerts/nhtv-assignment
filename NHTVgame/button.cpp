/**
 * scene_input class describes Button behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "Button.h"

Button::Button(std::string message, Point2 textpos, Point2 size, float lineWidth, RGBAColor color, RGBAColor colorhover, Input* Scene_Input) : Entity()
{
	// Set size (pixels) to local Scale
	Scale = size;
	// Scene input to scene_input
	scene_input = Scene_Input;
	// Set standard isClicked to false
	isClicked = false;

	// Set colors
	this->color = color;
	this->colorhover = colorhover;

	// Setting Button
	line = new Line_Entity();
	// Adding points to Line
	line->_line->addPoint(size.x, size.y);
	line->_line->addPoint(size.x, -size.y);
	line->_line->addPoint(-size.x, -size.y);
	line->_line->addPoint(-size.x, size.y);
	line->_line->closed(true);
	// Bind all points
	line->BindPoints(color, lineWidth);

	// Set Text
	text = new Text();
	// Set the message
	text->message(message);
	text->position = Point2(textpos.x, textpos.y);

	// Adding all childs to the scene (draw on window)
	this->addChild(text);
	this->addChild(line);
}

Button::~Button()
{
	// deconstruct and delete line
	if (line != NULL) {
		this->removeChild(line);
		delete line;
		line = NULL;
	}
}

void Button::update(float deltaTime)
{
	// ###############################################################
	// isDisabled make the button GRAY and not clickable
	// ###############################################################
	if (isDisabled) {
		// Set frame (line) to GRAY
		this->line->line()->color = GRAY;

		// Set all characters from the text to GRAY
		for (int i = 0; i < this->text->spritebatch().size(); i++) {
			this->text->spritebatch()[i]->color = GRAY;
		}
	}
	else {
		// If mouse if between the line of this Button
		if ((scene_input->getMouseX() <= this->position.x + Scale.x) &&
			(scene_input->getMouseX() >= this->position.x - Scale.x) &&
			(scene_input->getMouseY() <= this->position.y + Scale.y) &&
			(scene_input->getMouseY() >= this->position.y - Scale.y)) {

			// When hovering changes colors
			this->line->line()->color = color;
			for (int i = 0; i < this->text->spritebatch().size(); i++) {
				this->text->spritebatch()[i]->color = colorhover;
			}

			// If left button of the mouse if down
			if (scene_input->getMouse(0)) {
				// Set all colors to WHITE
				this->line->line()->color = WHITE;
				for (int i = 0; i < this->text->spritebatch().size(); i++) {
					this->text->spritebatch()[i]->color = WHITE;
				}
			}

			// If left button of the mouse if up
			if (scene_input->getMouseUp(0)) {
				// Set isClicked to true
				isClicked = true;
			}
		}
		else {
			// If mouse if not between the line of this Button, set colors back to normal
			this->line->line()->color = colorhover;

			for (int i = 0; i < this->text->spritebatch().size(); i++) {
				this->text->spritebatch()[i]->color = color;
			}
		}
	}
}

// ###############################################################
// Insert number of the newScene that you want to load
// ###############################################################
void Button::LoadNewSceneWhenClicked(int newScene) {
	if (isClicked) {
		SuperScene::activescene = newScene;
		isClicked = false;
	}
}