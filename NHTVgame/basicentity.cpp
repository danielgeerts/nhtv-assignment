/**
 * This class describes BasicEntity behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "BasicEntity.h"

BasicEntity::BasicEntity() : Entity()
{
}

BasicEntity::~BasicEntity()
{
}

void BasicEntity::update(float deltaTime)
{
}
