/**
 * This class describes Particle behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "Particle.h"

Particle::Particle(int bombpart, int grenadepart) : Entity()
{
	// ###############################################################
	// set (standard) cube sprite
	// ###############################################################
	if (bombpart == -1 && grenadepart == -1) {
		// Set sprite to Cube.tga
		this->addSprite("assets/cube.tga");
		this->scale = Point2(0.8f, 0.8f);
		changeColor = true;
	}

	// ###############################################################
	// set bomb sprites
	// ###############################################################
	if (bombpart == 0 && grenadepart == -1) {
		// Set sprite to bombpart0.tga
		this->addSprite("assets/bombpart0.tga");
		this->scale = Point2(0.8f, 0.8f);
	}
	if (bombpart == 1 && grenadepart == -1) {
		// Set sprite to bombpart1.tga
		this->addSprite("assets/bombpart1.tga");
		this->scale = Point2(0.8f, 0.8f);
	}
	if (bombpart == 2 && grenadepart == -1) {
		// Set sprite to bombpart2.tga
		this->addSprite("assets/bombpart2.tga");
		this->scale = Point2(0.8f, 0.8f);
	}

	// ###############################################################
	// set grenade sprites
	// ###############################################################
	if (bombpart == -1 && grenadepart == 0) {
		// Set sprite to grenadepart0.tga
		this->addSprite("assets/grenadepart0.tga");
		this->scale = Point2(0.8f, 0.8f);
	}
	if (bombpart == -1 && grenadepart == 1) {
		// Set sprite to grenadepart1.tga
		this->addSprite("assets/grenadepart1.tga");
		this->scale = Point2(0.8f, 0.8f);
	}
	if (bombpart == -1 && grenadepart == 2) {
		// Set sprite to grenadepart2.tga
		this->addSprite("assets/grenadepart2.tga");
		this->scale = Point2(0.8f, 0.8f);
	}

	// Standard set velocity to 0,0
	velocity = Point2(0, 0);

	// Give particle random velocity
	float tempX = rand() % 100 - 50;
	float tempY = rand() % 100 - 50;
	addableVelocity.x = tempX;
	addableVelocity.y = tempY;
	// addableVelocty times 2, so the particles will move faster
	addableVelocity *= 2.0f;

	// Give particle random rotation
	float newRotation = rand() % 720 - 360;
	newRotation = newRotation / 50;
	this->rotateDirection = newRotation;

	// if sprite if not a bomb or grenade part then change the color like a explosion
	if (changeColor) {
		// Red
		newColor = RGBAColor(255.0f, 0.0f, 0.0f);
		toYellow = true;
		toGray = false;
		newgreen = newColor.r;
		newgreen = newColor.g;
	}

	// Set trigger true, so the bomb or grenade sprite will be removed
	this->trigger = true;
}

Particle::~Particle()
{
}

void Particle::update(float deltaTime)
{
	// ###############################################################
	// Change color when the sprite is cube.tga
	// From red to yellow to gray (like a explosion)
	// ###############################################################
	if (changeColor) {
		if (toYellow) {
			newgreen += (218.0f / 2) * deltaTime;

			newColor.g = newgreen;
			if (newColor.g >= 218) {
				newColor.g = 218;
				toYellow = false;
				toGray = true;

				newred = newColor.r;
				newgreen = newColor.g;
			}
		}
		if (toGray) {
			newred -= (255.0f / 2) * deltaTime;

			newColor.r = newred;
			if (newColor.r <= 56) {
				newColor.r = 56;
				newColor.g = 56;
				newColor.b = 56;
				red = true;
			}
			newgreen -= (218.0f / 2) * deltaTime;

			newColor.g = newgreen;
			if (newColor.g <= 56) {
				newColor.r = 56;
				newColor.g = 56;
				newColor.b = 56;
				green = true;
			}

			if (red && green) {
				toGray = false;
			}

			// Check if every RGB value is 0
			if (!toYellow && !toGray) {
				needToDestroy = true;
			}
		}

		if (needToDestroy) {
			newColor.r == 56;
			newColor.g == 56;
			newColor.b == 56;
		}

		// Set once per frame the newcolor
		this->sprite()->color = newColor;
	}

	// ###############################################################
	// Change (total) velocity with the addableVelocity
	// ###############################################################
	velocity += addableVelocity * deltaTime;

	// ###############################################################
	// Sprite will rotate
	// ###############################################################
	this->rotation += rotateDirection * deltaTime;
}
