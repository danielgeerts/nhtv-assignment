/**
 * This class describes Player behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "Player.h"

Player::Player(Items CurrentItem) : Entity()
{
	// Set currenItem
	currenItem = CurrentItem;

	// Set currentItemHolding, as type of currenItem
	if (currenItem == BOMB) {
		currentItemHolding = new Bomb();
	} else if (currenItem == GRENADE) {
		currentItemHolding = new Grenade();
	}

	// Add currentItemHolding to the player
	this->addChild(currentItemHolding);
}

Player::~Player()
{
	// deconstruct and delete currentItemHolding
	if (currentItemHolding != NULL) {
		this->removeChild(currentItemHolding);
		delete currentItemHolding;
		currentItemHolding = NULL;
	}
}

void Player::update(float deltaTime)
{
}

// ###############################################################
// Changed the item that you are holding
// ###############################################################
void Player::ChangeHoldingItem(Items CurrentItem) {
	// deconstruct and delete currentItemHolding
	this->removeChild(currentItemHolding);
	delete currentItemHolding;
	currentItemHolding = NULL;
	
	// Set currentItemHolding as new type of CurrentItem
	if (CurrentItem == BOMB) {
		currentItemHolding = new Bomb();
	} else if (CurrentItem == GRENADE) {
		currentItemHolding = new Grenade();
	}

	// add currentItemHolding again to the scene
	currenItem = CurrentItem;
	if (currentItemHolding != NULL) {
		this->addChild(currentItemHolding);
	}
}