/**
 * This class describes Explosion behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "Explosion.h"

Explosion::Explosion(bool isBomb) : BasicEntity()
{
	// Create new particle system
	particlesystem = new ParticleSystem(isBomb);
	this->addChild(particlesystem);
}

Explosion::~Explosion()
{
	// deconstruct and delete particlesystem
	if (particlesystem != NULL) {
		this->removeChild(particlesystem);
		delete particlesystem;
		particlesystem = NULL;
	}
}

void Explosion::update(float deltaTime)
{
	// Destory particlesystem when needed
	if (particlesystem != NULL) {
		if (particlesystem->needToDestroySystem) {
			this->removeChild(particlesystem);
			delete particlesystem;
			particlesystem = NULL;
		}
	}
}