/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Play_Board.h
 */

#ifndef PLAY_BOARD_H
#define PLAY_BOARD_H

#include <map>

#include <rt2d/entity.h>
#include <rt2d/input.h>
#include <rt2d/text.h>

 // Include NHTVgame files
#include "line_entity.h"

/// @brief The Play_Board class is the Entity implementation.
class Play_Board : public Entity
{
public:
	/// @brief Constructor
	Play_Board(int scale, Input* Scene_Input);
	/// @brief Destructor
	virtual ~Play_Board();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief array with Point2 (x, y) with all possible boxes
	std::vector<Point2> Board_Items_Positions;
	/// @brief dictionary with all array Point2 from 'Board_Items_Positions'
	/// with bool, if false then the current box is empty
	/// if true then there is an item placed on that position
	std::map<Point2*, bool> Board_Items_Placed;

	/// @brief Get Board position when mouse hover
	int GetMouseHoverBoardItemPosition();

	/// @brief Current linewidth of the play_board
	float lineWidth = 1.0f;

private:
	/* add your private declarations */
	/// @brief all horizontal Lines
	Line_Entity* hori_left_line;
	Line_Entity* hori_right_line;

	/// @brief all vertical Lines
	Line_Entity* verti_up_line;
	Line_Entity* verti_down_line;

	/// @brief Scale in pixels
	int Scale;

	/// @brief Mouse hover over box position
	int mouseHoverBoardItemPosition = -1;

	/// @brief Mouse position to Text
	Text* mousePos;
	/// @brief scene_input is the mouse and keyboard Inut from the current Scene
	Input* scene_input;
};

#endif /* Play_Board_H */
