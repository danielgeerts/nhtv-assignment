/**
 * This class describes Scene_Level_vs_PC behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include <fstream>
#include <sstream>

#include "Scene_Level_vs_PC.h"

Scene_Level_vs_PC::Scene_Level_vs_PC() : Scene_Level()
{
	// Set all Scene_Level Settings
	text[1]->message("Scene Level - Player 1 vs Computer");
	text[6]->message("Click on a box on the playboard,");
	text[7]->message("to place the current item");

	// Set all characters from text[6] its Color to DARKBLUE 
	for (int i = 0; i < text[6]->spritebatch().size(); i++) {
		text[6]->spritebatch()[i]->color = NHTVcolorDARK;
	}
	// Set all characters from text[7] its Color to DARKBLUE 
	for (int i = 0; i < text[7]->spritebatch().size(); i++) {
		text[7]->spritebatch()[i]->color = NHTVcolorDARK;
	}

	// When player of computer starts set Text[9]
	if (checkIfComputerHasWon) {
		text[9]->message("Current player: BOMB - player 1");
	} else if (!checkIfComputerHasWon) {
		text[9]->message("Current player: GRENADE - computer");
	}
	// Set all characters from text[9] its Color to LIGHTBLUE 
	for (int i = 0; i < text[9]->spritebatch().size(); i++) {
		text[9]->spritebatch()[i]->color = NHTVcolorLIGHT;
	}

	// Set all wins to 0 when this class is created
	playerWins = 0;
	computerWins = 0;
	nobodyWins = 0;

	// Show all wins
	text[12]->message("Player wins: " + std::to_string(playerWins));
	text[13]->message("Computer wins: " + std::to_string(computerWins));
	text[14]->message("Draw: " + std::to_string(nobodyWins));
}


Scene_Level_vs_PC::~Scene_Level_vs_PC()
{
}

void Scene_Level_vs_PC::update(float deltaTime)
{
	Scene_Level::update(deltaTime);

	// ###############################################################
	// Check if the current Player is player1 of the computer
	// If the current Player is player1 and the level has not finished yet, set current item to BOMB
	// else if current Player is player1 and the level is finished, set current item to NONE
	// ###############################################################
	if (!computersTurn && !computersLevelFinished) {
		currentPlayer->ChangeHoldingItem(BOMB);
	} else {
		currentPlayer->ChangeHoldingItem(NONE);
	}

	// ###############################################################
	// If player has clicked on the play_board or need to check if the computer has won
	// When clicked on the play_board then place a item on the play_board at the specifiek position
	// Then check if someone has won or not
	// ###############################################################
	if (mouseClickOnBoard || checkIfComputerHasWon) {
		if (mouseClickOnBoard) {
			if (currentPlayer->currenItem == BOMB) {
				Bomb* tempbomb = new Bomb();
				tempbomb->position = play_board->Board_Items_Positions[mouseClickOnInt] / 0.8f;
				play_board->addChild(tempbomb);
				play_board->Board_Items_Placed[&play_board->Board_Items_Positions[mouseClickOnInt]] = true;

				ItemsAddedToPlayBoard.push_back(tempbomb);

				computersTurn = true;
				t.start();
			}
		}

		// Check if someone has won
		int result = checkWin();
		// Someone has won, check who won and act to who won
		if (result == 1) {
			// We have a winner, PLAYER current
			if (!checkIfComputerHasWon) {
				winnerText->message("Winner: Player");
				playerWins++;
			}
			else if (checkIfComputerHasWon) {
				winnerText->message("Winner: Computer");
				computerWins++;
			}
			text[12]->message("Player wins: " + std::to_string(playerWins));
			text[13]->message("Computer wins: " + std::to_string(computerWins));

			for (int i = 0; i < ItemsAddedToPlayBoard.size(); i++) {
				this->ItemsAddedToPlayBoard[i]->trigger = true;
			}

			computersLevelFinished = true;
			currentPlayer->ChangeHoldingItem(NONE);
			play_again->isDisabled = false;
			showWinner = true;
		}
		// Nobody has won
		else if (result == 0) {
			// :( No winner
			nobodyWins++;
			text[14]->message("Draw: " + std::to_string(nobodyWins));

			for (int i = 0; i < ItemsAddedToPlayBoard.size(); i++) {
				this->ItemsAddedToPlayBoard[i]->trigger = true;
			}

			computersLevelFinished = true;
			currentPlayer->ChangeHoldingItem(NONE);
			play_again->isDisabled = false;
			winnerText->message("Winner: Nobody");
			showWinner = true;
		}
		// Still playing the game
		else if (result == -1) {
			// Still playing

			currentPlayer->position.x = input()->getMouseX();
			currentPlayer->position.y = input()->getMouseY();
		}

		if (checkIfComputerHasWon) {
			text[9]->message("Current player: BOMB - player 1");
		}
		else if (!checkIfComputerHasWon) {
			text[9]->message("Current player: GRENADE - computer");
		}
		for (int i = 0; i < text[9]->spritebatch().size(); i++) {
			text[9]->spritebatch()[i]->color = NHTVcolorLIGHT;
		}

		checkIfComputerHasWon = false;
	}

	// If the computer is playing, wait for 0.5s so it looks normal, then place a item
	if (computersTurn) {
		if (t.seconds() >= 0.3f) {
			placeByComputer();
		}
	}

	// Show the winner Text, after 4.0s make the text 'disappear' (clear the message)
	if (showWinner) {
		if (!t.started()) {
			t.start();
		}
		if (t.seconds() >= 6.0f) {
			winnerText->message("");
			t.stop();
			showWinner = false;
		}
	}
}

// ###############################################################
// Place a item by the computer
// ###############################################################
void Scene_Level_vs_PC::placeByComputer() {
	if (!computersLevelFinished) {
		std::vector<int> emptyBoxesPositions;
		int computerPlacingBox = 0;

		// Check which position is still empty, so the computer will not place it above another item
		for (int i = 0; i < play_board->Board_Items_Placed.size(); i++) {
			if (play_board->Board_Items_Placed[&play_board->Board_Items_Positions[i]] == false) {
				emptyBoxesPositions.push_back(i);
			}
		}

		// Get random place to place the computers item
		if (emptyBoxesPositions.size() != 0) {
			srand(time(NULL));
			computerPlacingBox = rand() % emptyBoxesPositions.size();
		}
		computerPlacingBox = emptyBoxesPositions[computerPlacingBox];

		// Set the first item in the middle of the play_board
		if (play_board->Board_Items_Placed[&play_board->Board_Items_Positions[4]] == false) {
			computerPlacingBox = 4;
		}
		// When the computer can win or lose
		if (computerController() != -1) {
			computerPlacingBox = computerController();
		}

		// Make Item to place and add it to the play_board
		Grenade* tempgrenade = new Grenade();
		tempgrenade->position = play_board->Board_Items_Positions[computerPlacingBox] / 0.8f;
		play_board->addChild(tempgrenade);
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[computerPlacingBox]] = true;

		// Add item to the placed items array
		ItemsAddedToPlayBoard.push_back(tempgrenade);

		// Current item placed box set to GRENADE
		placedItems[computerPlacingBox] = GRENADE;

		// Check if the computer has won
		checkIfComputerHasWon = true;
	}

	// Set computers turn to false, so the player can play again
	computersTurn = false;
}


// ###############################################################
// Controller where the computer is placing a item
// TODO: make shorter
// ###############################################################
int Scene_Level_vs_PC::computerController() {
	// First check if you have a WIN possibility
	// Top row
	if (placedItems[0] == NONE && placedItems[1] == GRENADE && placedItems[2] == GRENADE) {
		return 0;
	}
	else if (placedItems[0] == GRENADE && placedItems[1] == NONE && placedItems[2] == GRENADE) {
		return 1;
	}
	else if (placedItems[0] == GRENADE && placedItems[1] == GRENADE && placedItems[2] == NONE) {
		return 2;
	}
	// Middle row
	else if (placedItems[3] == NONE && placedItems[4] == GRENADE && placedItems[5] == GRENADE) {
		return 3;
	}
	else if (placedItems[3] == GRENADE && placedItems[4] == NONE && placedItems[5] == GRENADE) {
		return 4;
	}
	else if (placedItems[3] == GRENADE && placedItems[4] == GRENADE && placedItems[5] == NONE) {
		return 5;
	}
	// Bottom row
	else if (placedItems[6] == NONE && placedItems[7] == GRENADE && placedItems[8] == GRENADE) {
		return 6;
	}
	else if (placedItems[6] == GRENADE && placedItems[7] == NONE && placedItems[8] == GRENADE) {
		return 7;
	}
	else if (placedItems[6] == GRENADE && placedItems[7] == GRENADE && placedItems[8] == NONE) {
		return 8;
	}
	// Left row
	else if (placedItems[0] == NONE && placedItems[3] == GRENADE && placedItems[6] == GRENADE) {
		return 0;
	}
	else if (placedItems[0] == GRENADE && placedItems[3] == NONE && placedItems[6] == GRENADE) {
		return 3;
	}
	else if (placedItems[0] == GRENADE && placedItems[3] == GRENADE && placedItems[6] == NONE) {
		return 6;
	}
	// Middle row
	else if (placedItems[1] == NONE && placedItems[4] == GRENADE && placedItems[7] == GRENADE) {
		return 1;
	}
	else if (placedItems[1] == GRENADE && placedItems[4] == NONE && placedItems[7] == GRENADE) {
		return 4;
	}
	else if (placedItems[1] == GRENADE && placedItems[4] == GRENADE && placedItems[7] == NONE) {
		return 7;
	}
	// Right row
	else if (placedItems[2] == NONE && placedItems[5] == GRENADE && placedItems[8] == GRENADE) {
		return 2;
	}
	else if (placedItems[2] == GRENADE && placedItems[5] == NONE && placedItems[8] == GRENADE) {
		return 5;
	}
	else if (placedItems[2] == GRENADE && placedItems[5] == GRENADE && placedItems[8] == NONE) {
		return 8;
	}
	// Diagonal 1 row
	else if (placedItems[0] == NONE && placedItems[4] == GRENADE && placedItems[8] == GRENADE) {
		return 0;
	}
	else if (placedItems[0] == GRENADE && placedItems[4] == NONE && placedItems[8] == GRENADE) {
		return 4;
	}
	else if (placedItems[0] == GRENADE && placedItems[4] == GRENADE && placedItems[8] == NONE) {
		return 8;
	}
	// Diagonal 2 row
	else if (placedItems[2] == NONE && placedItems[4] == GRENADE && placedItems[6] == GRENADE) {
		return 2;
	}
	else if (placedItems[2] == GRENADE && placedItems[4] == NONE && placedItems[6] == GRENADE) {
		return 4;
	}
	else if (placedItems[2] == GRENADE && placedItems[4] == GRENADE && placedItems[6] == NONE) {
		return 6;
	}

	// Second check if BOMB have a WIN possibility, so the computer can disable this win

	// Top row
	else if (placedItems[0] == NONE && placedItems[1] == BOMB && placedItems[2] == BOMB) {
		return 0;
	}
	else if (placedItems[0] == BOMB && placedItems[1] == NONE && placedItems[2] == BOMB) {
		return 1;
	}
	else if (placedItems[0] == BOMB && placedItems[1] == BOMB && placedItems[2] == NONE) {
		return 2;
	}
	// Middle row
	else if (placedItems[3] == NONE && placedItems[4] == BOMB && placedItems[5] == BOMB) {
		return 3;
	}
	else if (placedItems[3] == BOMB && placedItems[4] == NONE && placedItems[5] == BOMB) {
		return 4;
	}
	else if (placedItems[3] == BOMB && placedItems[4] == BOMB && placedItems[5] == NONE) {
		return 5;
	}
	// Bottom row
	else if (placedItems[6] == NONE && placedItems[7] == BOMB && placedItems[8] == BOMB) {
		return 6;
	}
	else if (placedItems[6] == BOMB && placedItems[7] == NONE && placedItems[8] == BOMB) {
		return 7;
	}
	else if (placedItems[6] == BOMB && placedItems[7] == BOMB && placedItems[8] == NONE) {
		return 8;
	}
	// Left row
	else if (placedItems[0] == NONE && placedItems[3] == BOMB && placedItems[6] == BOMB) {
		return 0;
	}
	else if (placedItems[0] == BOMB && placedItems[3] == NONE && placedItems[6] == BOMB) {
		return 3;
	}
	else if (placedItems[0] == BOMB && placedItems[3] == BOMB && placedItems[6] == NONE) {
		return 6;
	}
	// Middle row
	else if (placedItems[1] == NONE && placedItems[4] == BOMB && placedItems[7] == BOMB) {
		return 1;
	}
	else if (placedItems[1] == BOMB && placedItems[4] == NONE && placedItems[7] == BOMB) {
		return 4;
	}
	else if (placedItems[1] == BOMB && placedItems[4] == BOMB && placedItems[7] == NONE) {
		return 7;
	}
	// Right row
	else if (placedItems[2] == NONE && placedItems[5] == BOMB && placedItems[8] == BOMB) {
		return 2;
	}
	else if (placedItems[2] == BOMB && placedItems[5] == NONE && placedItems[8] == BOMB) {
		return 5;
	}
	else if (placedItems[2] == BOMB && placedItems[5] == BOMB && placedItems[8] == NONE) {
		return 8;
	}
	// Diagonal 1 row
	else if (placedItems[0] == NONE && placedItems[4] == BOMB && placedItems[8] == BOMB) {
		return 0;
	}
	else if (placedItems[0] == BOMB && placedItems[4] == NONE && placedItems[8] == BOMB) {
		return 4;
	}
	else if (placedItems[0] == BOMB && placedItems[4] == BOMB && placedItems[8] == NONE) {
		return 8;
	}
	// Diagonal 2 row
	else if (placedItems[2] == NONE && placedItems[4] == BOMB && placedItems[6] == BOMB) {
		return 2;
	}
	else if (placedItems[2] == BOMB && placedItems[4] == NONE && placedItems[6] == BOMB) {
		return 4;
	}
	else if (placedItems[2] == BOMB && placedItems[4] == BOMB && placedItems[6] == NONE) {
		return 6;
	}

	return -1;
}

// ###############################################################
// Called when 'play_again' is clicked, from extender Scene_Level
// ###############################################################
void Scene_Level_vs_PC::restartLevel() {
	Scene_Level::restartLevel();
	// do stuff like empty vector's and make some variables 0 again
	// maybe give 'delete' some variable and set then again to a 'new' variable
	winnerText->message("");
	play_again->isDisabled = true;

	// Set Text[9] to current player BOMB - player 1
	text[9]->message("Current player: BOMB - player 1");

	// Set all characters from text[9] its Color to LIGHTBLUE 
	for (int i = 0; i < text[9]->spritebatch().size(); i++) {
		text[9]->spritebatch()[i]->color = NHTVcolorLIGHT;
	}
}
