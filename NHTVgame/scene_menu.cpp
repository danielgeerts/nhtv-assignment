/**
 * This class describes Scene_Menu behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include <fstream>
#include <sstream>

#include "Scene_Menu.h"

Scene_Menu::Scene_Menu() : SuperScene()
{
	// Set all SuperScene Settings
	text[0]->message("Press ESC to quit");
	text[1]->message("Scene Menu - Awesome game made by Daniel Geerts, assignment from NHTV");
	this->showMousePosition = false;

	// Setting child Level_1v1
	Level_1v1 = new Button("1 vs 1", Point2(-80, 0), Point2(120.0f, 35.0f), 5.0f, NHTVcolorLIGHT, NHTVcolorDARK, input());
	Level_1v1->position = Point2(SWIDTH / 4 * 1, SHEIGHT / 4 * 1.8f);

	// Setting child play_board
	Level_1vCom = new Button("1 vs computer", Point2(-190, 0), Point2(230.0f, 35.0f), 5.0f, NHTVcolorLIGHT, NHTVcolorDARK, input());
	Level_1vCom->position = Point2(SWIDTH / 4 * 3, SHEIGHT / 4 * 1.8f);

	// Setting child title
	title = new Text();
	title->message("Tic Tac Boom!");
	title->position = Point2(300, SHEIGHT / 4 * 1.0f);
	title->scale = Point2(2.0f, 2.0f);

	// Set all characters colors to LIGHTBLUE
	for (int i = 0; i < title->spritebatch().size(); i++) {
		title->spritebatch()[i]->color = NHTVcolorDARK;
	}

	// Add lines to the credits Text
	for (unsigned int i = 0; i < 7; i++) {
		Text* line = new Text();
		line->scale = Point2(0.5f, 0.5f);
		line->position = Point2(50, SHEIGHT / 4 * 2.55f + i * 35);
		line->message("", NHTVcolorLIGHT);

		credits.push_back(line);
		this->addChild(line);
	}

	// Fill each Text in credits with a string
	credits[0]->message("Programming assignment, NHTV, Creative Media and Game Technologies 2017/2018.");
	credits[1]->message("This programming assignment is made by Daniel Geerts, Student number - 170042.");
	credits[2]->message("All the code from 'NHTVgame' and all the art from 'NHTVgame/assets' I made.");
	credits[3]->message("===============================================================================");
	credits[4]->message("The framework (library) I used:");
	credits[5]->message("    - OpenGL (glew, glfw, glm)        http://www.opengl-tutorial.org/");
	credits[6]->message("    - RT2D                            https://github.com/rktrlng/rt2d");

	// Set every character of the credits array to DARKBLUE
	for (int i = 0; i < credits.size(); i++) {
		if (i != 3) {
			for (int j = 0; j < credits[i]->spritebatch().size(); j++) {
				credits[i]->spritebatch()[j]->color = NHTVcolorDARK;
			}
		} else {
			for (int j = 0; j < credits[i]->spritebatch().size(); j++) {
				credits[i]->spritebatch()[j]->color = NHTVcolorLIGHT;
			}
		}
	}

	// Adding childs to the scene
	this->addChild(Level_1v1);
	this->addChild(Level_1vCom);
	this->addChild(title);
}


Scene_Menu::~Scene_Menu()
{
	// deconstruct and delete Level_1v1
	if (Level_1v1 != NULL) {
		this->removeChild(Level_1v1);
		delete Level_1v1;
		Level_1v1 = NULL;
	}

	// deconstruct and delete Level_1vCom
	if (Level_1vCom != NULL) {
		this->removeChild(Level_1vCom);
		delete Level_1vCom;
		Level_1vCom = NULL;
	}

	// deconstruct and delete title
	if (title != NULL) {
		this->removeChild(title);
		delete title;
		title = NULL;
	}

	// deconstruct and delete credits
	for (int i = 0; i < credits.size(); i++) {
		this->removeChild(credits[i]);
		delete credits[i];
		credits[i] = NULL;
	}
}

void Scene_Menu::update(float deltaTime)
{
	SuperScene::update(deltaTime);

	// Check each frame if a button is clicked
	Level_1v1->LoadNewSceneWhenClicked(1);
	Level_1vCom->LoadNewSceneWhenClicked(2);
}