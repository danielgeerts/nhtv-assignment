/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file ParticleSystem.h
 */

#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include <rt2d/entity.h>

 // Include NHTVgame files
#include "particle.h"

/// @brief The ParticleSystem class is the Entity implementation.
class ParticleSystem : public Entity
{
public:
	/// @brief Constructor
	ParticleSystem(bool isBomb);
	/// @brief Destructor
	virtual ~ParticleSystem();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief Boolean 'trigger' can be used in extended files from ParticleSystem files for a trigger
	/// For example: You can acces this trigger from a array with the type ParticleSystem and use
	/// this trigger in the extended files, after use set this to false
	bool trigger = false;

	/// @brief Add new particle to this particle system
	void addNewParticle(int bombpart, int grenadepart);

	/// @brief List of all particles in this particle system
	std::vector<Particle*> particles;

	/// @brief When true, this particle system with all particles will be destroyed
	bool needToDestroySystem = false;
};

#endif /* PARTICLESYSTEM_H */
