/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Scene_Level_vs_PC.h
 */

#ifndef SCENE_LEVEL_VS_PC_H
#define SCENE_LEVEL_VS_PC_H

// Include NHTVgame files
#include "Scene_Level.h"

/// @brief The Scene_Level_vs_PC class is the Scene implementation.
class Scene_Level_vs_PC : public Scene_Level
{
public:
	/// @brief Constructor
	Scene_Level_vs_PC();
	/// @brief Destructor
	virtual ~Scene_Level_vs_PC();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief restart level when 'play_again' is clicked
	virtual void restartLevel();

private:
	/// @brief child Timer t
	Timer t;

	/// @brief Place a item by the computer
	void placeByComputer();
	/// @brief Controller of the computer
	int computerController();

	/// @brief amout of player wins
	int playerWins;
	/// @brief amout of computer wins
	int computerWins;
	/// @brief amout of draws
	int nobodyWins;
};

#endif /* SCENE_LEVEL_VS_PC_H */
