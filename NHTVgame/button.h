/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Button.h
 */

#ifndef BUTTON_H
#define BUTTON_H

#include <rt2d/entity.h>
#include <rt2d/input.h>
#include <rt2d/text.h>

#include "superscene.h"
#include "line_entity.h"

/// @brief The Button class is the Entity implementation.
class Button : public Entity
{
public:
	/// @brief Constructor
	Button(std::string message, Point2 textpos, Point2 size, float lineWidth, RGBAColor color, RGBAColor colorhover, Input* Scene_Input);
	/// @brief Destructor
	virtual ~Button();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief Insert the new scene ID to load this scene
	void LoadNewSceneWhenClicked(int newScene);

	/// @brief isDisabled or not
	bool isDisabled = false;
	/// @brief isClicked or not
	bool isClicked = false;

private:
	/* add your private declarations */

	/// @brief Line for the frame of this button
	Line_Entity* line;
	/// @brief Scale of the button (in pixels)
	Point2 Scale;
	/// @brief Get (single) input from the current Scene
	Input* scene_input;
	/// @brief text for in the Button
	Text* text;

	/// @brief Colors for the Text and Frame
	RGBAColor color = WHITE;
	RGBAColor colorhover = BLACK;
};

#endif /* BUTTON_H */
