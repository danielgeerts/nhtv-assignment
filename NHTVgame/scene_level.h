/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Scene_Level.h
 */

#ifndef SCENE_LEVEL_H
#define SCENE_LEVEL_H

#include <rt2d/scene.h>

// Include NHTVgame files
#include "superscene.h"
#include "basicentity.h"
#include "play_board.h"
#include "player.h"
#include "explosion.h"
#include "button.h"

/// @brief The Scene_Level class is the Scene implementation.
class Scene_Level : public SuperScene
{
public:
	/// @brief Constructor
	Scene_Level();
	/// @brief Destructor
	virtual ~Scene_Level();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief restart level
	virtual void restartLevel();

protected:
	/// @brief Funtion checking if someone has won
	int checkWin();

	/// @brief child play_board
	Play_Board* play_board;
	/// @brief child currentPlayer
	Player* currentPlayer;

	/// @brief when true mouse has clicked on board
	bool mouseClickOnBoard;
	/// @brief current box mouse if hovering above
	int mouseClickOnInt;

	/// @brief int is the box number, Items is the current item placed (standard, NONE)
	std::map<int, Items> placedItems;
	/// @brief all placed items on the board
	std::vector<BasicEntity*> ItemsAddedToPlayBoard;

	/// @brief Set variables for the computers turn
	bool computersTurn;
	bool checkIfComputerHasWon;
	bool computersLevelFinished;

	/// @brief play level again button
	Button* play_again;

	/// @brief 'Who won? You will see it here' Text
	Text* winnerText;
	/// @brief When true, show the 'winnerText'
	bool showWinner = false;
};

#endif /* SCENE_LEVEL_H */
