/**
 * This class describes Scene_Level behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include <fstream>
#include <sstream>

#include "Scene_Level.h"

Scene_Level::Scene_Level() : SuperScene()
{
	// Set all SuperScene Settings
	text[0]->message("Press ESC to go to the menu Scene");
	text[1]->message("Scene Level - NOT WORKING PROPERLY");
	this->showMousePosition = true;

	// Setting child play_board
	play_board = new Play_Board(250, input());
	play_board->position = Point2(SWIDTH / 4 * 2.75f, SHEIGHT / 4 * 2);
	play_board->scale = Point2(0.8f, 0.8f);

	// Setting child currentPlayer
	currentPlayer = new Player(BOMB);

	// Setting child play_again
	play_again = new Button("Play again", Point2(-140, 0), Point2(180.0f, 35.0f), 10.0f, NHTVcolorLIGHT, NHTVcolorDARK, input());
	play_again->position = Point2(SWIDTH / 4 * 0.75f, SHEIGHT / 4 * 3.5f);
	play_again->isDisabled = true;

	// Adding childs to the scene
	this->addChild(play_board);
	this->addChild(currentPlayer);
	this->addChild(play_again);

	// Add 9 boxes and set all boxes to NONE
	placedItems = {
		{ 0, NONE },
		{ 1, NONE },
		{ 2, NONE },
		{ 3, NONE },
		{ 4, NONE },
		{ 5, NONE },
		{ 6, NONE },
		{ 7, NONE },
		{ 8, NONE }
	};

	// Set variables for the computers turn
	computersTurn = false;
	computersLevelFinished = false;

	// Set and add winnerText to the scene
	winnerText = new Text();
	winnerText->position = Point2(50, SHEIGHT / 4 * 3 - 50);
	winnerText->message("");
	this->addChild(winnerText);
}


Scene_Level::~Scene_Level()
{
	// deconstruct and delete play_board
	if (play_board != NULL) {
		this->removeChild(play_board);
		delete play_board;
		play_board = NULL;
	}

	// deconstruct and delete currentPlayer
	if (currentPlayer != NULL) {
		this->removeChild(currentPlayer);
		delete currentPlayer;
		currentPlayer = NULL;
	}

	// deconstruct and delete play_again
	if (play_again != NULL) {
		this->removeChild(play_again);
		delete play_again;
		play_again = NULL;
	}
	
	// deconstruct and delete items in placedItems
	for (int i = 0; i < ItemsAddedToPlayBoard.size(); i++) {
		this->removeChild(ItemsAddedToPlayBoard[i]);
		delete ItemsAddedToPlayBoard[i];
		ItemsAddedToPlayBoard[i] = NULL;
	}

	// deconstruct and delete winnerText
	if (winnerText != NULL) {
		this->removeChild(winnerText);
		delete winnerText;
		winnerText = NULL;
	}
}

void Scene_Level::update(float deltaTime)
{
	SuperScene::update(deltaTime);
	// ###############################################################
	// Set every frame the local mouse variables, to not clicked
	// ###############################################################
	mouseClickOnBoard = false;
	mouseClickOnInt = -1;

	// ###############################################################
	// Check if the mouse if above a empty box and the mouse is clicked
	// Check if it is the computers turn
	// ###############################################################
	for (int i = 0; i < play_board->Board_Items_Positions.size(); i++) {
		if (play_board->GetMouseHoverBoardItemPosition() == i && currentPlayer->currenItem != NONE && !computersTurn) {
			if (play_board->Board_Items_Placed[&play_board->Board_Items_Positions[i]] == false) {
				currentPlayer->position = play_board->position + play_board->Board_Items_Positions[i];
				if (input()->getMouseDown(0)) {
					mouseClickOnBoard = true;
					mouseClickOnInt = i;

					placedItems[i] = currentPlayer->currenItem;
				}
			} else {
				currentPlayer->position.x = input()->getMouseX();
				currentPlayer->position.y = input()->getMouseY();
			}
		} else if (play_board->GetMouseHoverBoardItemPosition() == -1) {
			currentPlayer->position.x = input()->getMouseX();
			currentPlayer->position.y = input()->getMouseY();
		}
	}

	// ###############################################################
	// When play_again is clicked then restart the level
	// ###############################################################
	if (play_again->isDisabled == false && play_again->isClicked) {
		restartLevel();
		play_again->isClicked = false;
	}
	
	// Set text[4] to the mouse hover above the current box
	text[4]->message("Current Box: " + std::to_string(play_board->GetMouseHoverBoardItemPosition()));
}

// ###############################################################
// A very long function to check if there are 3 of the same kind item next to each other
// TODO: Make it sorter
// ###############################################################
int Scene_Level::checkWin() {
	if ((play_board->Board_Items_Placed[&play_board->Board_Items_Positions[0]] == true &&
			play_board->Board_Items_Placed[&play_board->Board_Items_Positions[1]] == true &&
			play_board->Board_Items_Placed[&play_board->Board_Items_Positions[2]] == true) &&
		(placedItems[0] == BOMB &&
			placedItems[1] == BOMB &&
			placedItems[2] == BOMB) ||
		(placedItems[0] == GRENADE &&
			placedItems[1] == GRENADE &&
			placedItems[2] == GRENADE))

		return 1;
	else if ((play_board->Board_Items_Placed[&play_board->Board_Items_Positions[3]] == true &&
			play_board->Board_Items_Placed[&play_board->Board_Items_Positions[4]] == true &&
			play_board->Board_Items_Placed[&play_board->Board_Items_Positions[5]] == true) &&
		(placedItems[3] == BOMB &&
			placedItems[4] == BOMB &&
			placedItems[5] == BOMB) ||
		(placedItems[3] == GRENADE &&
			placedItems[4] == GRENADE &&
			placedItems[5] == GRENADE))

		return 1;
	else if ((play_board->Board_Items_Placed[&play_board->Board_Items_Positions[6]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[7]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[8]] == true) &&
		(placedItems[6] == BOMB &&
			placedItems[7] == BOMB &&
			placedItems[8] == BOMB) ||
		(placedItems[6] == GRENADE &&
			placedItems[7] == GRENADE &&
			placedItems[8] == GRENADE))

		return 1;
	else if ((play_board->Board_Items_Placed[&play_board->Board_Items_Positions[0]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[3]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[6]] == true) &&
		(placedItems[0] == BOMB &&
			placedItems[3] == BOMB &&
			placedItems[6] == BOMB) ||
		(placedItems[0] == GRENADE &&
			placedItems[3] == GRENADE &&
			placedItems[6] == GRENADE))

		return 1;
	else if ((play_board->Board_Items_Placed[&play_board->Board_Items_Positions[1]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[4]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[7]] == true) &&
		(placedItems[1] == BOMB &&
			placedItems[4] == BOMB &&
			placedItems[7] == BOMB) ||
		(placedItems[1] == GRENADE &&
			placedItems[4] == GRENADE &&
			placedItems[7] == GRENADE))

		return 1;
	else if ((play_board->Board_Items_Placed[&play_board->Board_Items_Positions[2]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[5]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[8]] == true) &&
		(placedItems[2] == BOMB &&
			placedItems[5] == BOMB &&
			placedItems[8] == BOMB) ||
		(placedItems[2] == GRENADE &&
			placedItems[5] == GRENADE &&
			placedItems[8] == GRENADE))

		return 1;
	else if ((play_board->Board_Items_Placed[&play_board->Board_Items_Positions[0]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[4]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[8]] == true) &&
		(placedItems[0] == BOMB &&
			placedItems[4] == BOMB &&
			placedItems[8] == BOMB) ||
		(placedItems[0] == GRENADE &&
			placedItems[4] == GRENADE &&
			placedItems[8] == GRENADE))

		return 1;
	else if ((play_board->Board_Items_Placed[&play_board->Board_Items_Positions[2]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[4]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[6]] == true) &&
		(placedItems[2] == BOMB &&
			placedItems[4] == BOMB &&
			placedItems[6] == BOMB) ||
		(placedItems[2] == GRENADE &&
			placedItems[4] == GRENADE &&
			placedItems[6] == GRENADE))

		return 1;
	else if (play_board->Board_Items_Placed[&play_board->Board_Items_Positions[0]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[1]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[2]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[3]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[4]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[5]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[6]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[7]] == true &&
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[8]] == true)

		return 0;
	else
		return -1;
}

// ###############################################################
// Set some variables to standard values so it looks like that the level 'restarts'
// ###############################################################
void Scene_Level::restartLevel() {
	// Using a iterator to remove and erase all elements from the scene and from the 'ItemsAddedToPlayBoard'
	std::vector<BasicEntity*>::iterator iter = ItemsAddedToPlayBoard.begin();
	while (iter != ItemsAddedToPlayBoard.end())
	{
		play_board->removeChild(*iter);
		delete *iter;
		*iter = NULL;
		iter = ItemsAddedToPlayBoard.erase(iter);
	}
	ItemsAddedToPlayBoard = std::vector<BasicEntity*>();

	for (int i = 0; i < play_board->Board_Items_Placed.size(); i++) {
		play_board->Board_Items_Placed[&play_board->Board_Items_Positions[i]] = false;
	}

	currentPlayer->ChangeHoldingItem(BOMB);

	placedItems = {
		{ 0, NONE },
		{ 1, NONE },
		{ 2, NONE },
		{ 3, NONE },
		{ 4, NONE },
		{ 5, NONE },
		{ 6, NONE },
		{ 7, NONE },
		{ 8, NONE }
	};

	computersTurn = false;
	checkIfComputerHasWon = false;
	computersLevelFinished = false;
}