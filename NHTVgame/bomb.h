/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Bomb.h
 */

#ifndef BOMB_H
#define BOMB_H

// Include NHTVgame files
#include "basicentity.h"
#include "explosion.h"

/// @brief The Bomb class is the Entity implementation.
class Bomb : public BasicEntity
{
public:
	/// @brief Constructor
	Bomb();
	/// @brief Destructor
	virtual ~Bomb();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

private:
	/* add your private declarations */

	/// @brief Add the bomb sprite
	BasicEntity* bomb;
	/// @brief Add the explosion spritesheet
	Explosion* explosion;

	/// @brief Some variables for the explosion
	bool addExplosion = true;
};

#endif /* BOMB_H */
