/**
 * This class describes Bomb behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "Bomb.h"

Bomb::Bomb() : BasicEntity()
{
	// Setting sprite bomb
	bomb = new BasicEntity();
	bomb->addSprite("assets/bomb.tga");
	bomb->scale = Point2(0.75f, 0.75f);

	// Add sprite bomb to this BasicEntity
	this->addChild(bomb);

	// Setting spritesheet explosion
	explosion = new Explosion(true);
}

Bomb::~Bomb()
{
	// deconstruct and delete explosion
	if (explosion != NULL) {
		bomb->removeChild(explosion);
		delete explosion;
		explosion = NULL;
	}

	// deconstruct and delete bomb
	if (bomb != NULL) {
		this->removeChild(bomb);
		delete bomb;
		bomb = NULL;
	}
}

void Bomb::update(float deltaTime)
{
	// ###############################################################
	// trigger from BasicEntity
	// ###############################################################
	if (trigger) {
		// Add the explosion to the bomb, so this program will draw the exlosion
		if (explosion != NULL && addExplosion) {
			explosion->trigger = trigger;
			bomb->addChild(explosion);
			addExplosion = false;
		}

		// Remove bomb sprite when trigger is triggered
		if (explosion->particlesystem->trigger) {
			bomb->deleteSprite();
		}

		// Remove everything when the bomb is exploded
		if (explosion->particlesystem->particles.size() == 0 && !addExplosion) {
			if (explosion != NULL) {
				bomb->removeChild(explosion);
				delete explosion;
				explosion = NULL;
			}
			if (bomb != NULL) {
				this->removeChild(bomb);
				delete bomb;
				bomb = NULL;
			}
			trigger = false;
		}
	}
}
