/**
 * This class describes Scene_Level_vs_Player behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include <fstream>
#include <sstream>

#include "Scene_Level_vs_Player.h"

Scene_Level_vs_Player::Scene_Level_vs_Player() : Scene_Level()
{
	// Set all SuperScene Settings
	text[1]->message("Scene Level - Player 1 vs Player 2");
	text[6]->message("Click on a box on the playboard,");
	text[7]->message("to place the current item");

	// Set all characters from text[6] its Color to DARKBLUE 
	for (int i = 0; i < text[6]->spritebatch().size(); i++) {
		text[6]->spritebatch()[i]->color = NHTVcolorDARK;
	}
	// Set all characters from text[7] its Color to DARKBLUE 
	for (int i = 0; i < text[7]->spritebatch().size(); i++) {
		text[7]->spritebatch()[i]->color = NHTVcolorDARK;
	}

	// When bomb of grenade starts set Text[9]
	if (currentPlayer->currenItem == BOMB) {
		text[9]->message("Current player: Bomb");
	}
	else if (currentPlayer->currenItem == GRENADE) {
		text[9]->message("Current player: Grenade");
	}
	// Set all characters from text[9] its Color to LIGHTBLUE 
	for (int i = 0; i < text[9]->spritebatch().size(); i++) {
		text[9]->spritebatch()[i]->color = NHTVcolorLIGHT;
	}

	// Set all wins to 0 when this class is created
	bombWins = 0;
	grenadeWins = 0;
	nobodyWins = 0;

	// Show all wins
	text[12]->message("Bomb wins: " + std::to_string(bombWins));
	text[13]->message("Grenade wins: " + std::to_string(grenadeWins));
	text[14]->message("Draw: " + std::to_string(nobodyWins));
}


Scene_Level_vs_Player::~Scene_Level_vs_Player()
{
}

void Scene_Level_vs_Player::update(float deltaTime)
{
	Scene_Level::update(deltaTime);

	// ###############################################################
	// If player has clicked on the play_board
	// When clicked on the play_board then place a specifiek item on the play_board at the specifiek position
	// Then check if someone has won or not
	// ###############################################################
	if (mouseClickOnBoard) {
		if (currentPlayer->currenItem == BOMB) {
			Bomb* tempbomb = new Bomb();
			tempbomb->position = play_board->Board_Items_Positions[mouseClickOnInt] / 0.8f;
			play_board->addChild(tempbomb);
			play_board->Board_Items_Placed[&play_board->Board_Items_Positions[mouseClickOnInt]] = true;

			ItemsAddedToPlayBoard.push_back(tempbomb);

		} else if (currentPlayer->currenItem == GRENADE) {
			Grenade* tempgrenade = new Grenade();
			tempgrenade->position = play_board->Board_Items_Positions[mouseClickOnInt] / 0.8f;
			play_board->addChild(tempgrenade);
			play_board->Board_Items_Placed[&play_board->Board_Items_Positions[mouseClickOnInt]] = true;

			ItemsAddedToPlayBoard.push_back(tempgrenade);

		}

		// Check if someone has won
		int result = checkWin();
		// Someone has won, check who won and act to who won
		if (result == 1) {
			// We have a winner, PLAYER current
			if (currentPlayer->currenItem == BOMB) {
				winnerText->message("Winner: Bomb");
				bombWins++;
			} else if (currentPlayer->currenItem == GRENADE) {
				winnerText->message("Winner: Grenade");
				grenadeWins++;
			}
			text[12]->message("Bomb wins: " + std::to_string(bombWins));
			text[13]->message("Grenade wins: " + std::to_string(grenadeWins));

			for (int i = 0; i < ItemsAddedToPlayBoard.size(); i++) {
				this->ItemsAddedToPlayBoard[i]->trigger = true;
			}

			currentPlayer->ChangeHoldingItem(NONE);
			play_again->isDisabled = false;
			showWinner = true;
		}
		// Nobody has won
		else if (result == 0) {
			// :( No winner
			nobodyWins++;
			text[14]->message("Draw: " + std::to_string(nobodyWins));

			for (int i = 0; i < ItemsAddedToPlayBoard.size(); i++) {
				this->ItemsAddedToPlayBoard[i]->trigger = true;
			}

			currentPlayer->ChangeHoldingItem(NONE);
			play_again->isDisabled = false;
			winnerText->message("Winner: Nobody");
			showWinner = true;
		}
		// Still playing the game
		else if (result == -1) {
			// Still playing
			if (currentPlayer->currenItem == BOMB) {
				currentPlayer->ChangeHoldingItem(GRENADE);
			}
			else if (currentPlayer->currenItem == GRENADE) {
				currentPlayer->ChangeHoldingItem(BOMB);
			}

			currentPlayer->position.x = input()->getMouseX();
			currentPlayer->position.y = input()->getMouseY();
		}

		if (currentPlayer->currenItem == BOMB) {
			text[9]->message("Current player: Bomb");
		}
		else if (currentPlayer->currenItem == GRENADE) {
			text[9]->message("Current player: Grenade");
		}
		for (int i = 0; i < text[9]->spritebatch().size(); i++) {
			text[9]->spritebatch()[i]->color = NHTVcolorLIGHT;
		}
	}

	// Show the winner Text, after 4.0s make the text 'disappear' (clear the message)
	if (showWinner) {
		if (!t.started()) {
			t.start();
		}
		if (t.seconds() >= 6.0f) {
			winnerText->message("");
			t.stop();
			showWinner = false;
		}
	}
}


// ###############################################################
// Called when 'play_again' is clicked, from extender Scene_Level
// ###############################################################
void Scene_Level_vs_Player::restartLevel() {
	Scene_Level::restartLevel();
	// do stuff like empty vector's and make some variables 0 again
	// maybe give 'delete' some variable and set then again to a 'new' variable
	winnerText->message("");
	play_again->isDisabled = true;

	// Set Text[9] to current player BOMB
	text[9]->message("Current player: Bomb");

	// Set all characters from text[9] its Color to LIGHTBLUE 
	for (int i = 0; i < text[9]->spritebatch().size(); i++) {
		text[9]->spritebatch()[i]->color = NHTVcolorLIGHT;
	}
}
