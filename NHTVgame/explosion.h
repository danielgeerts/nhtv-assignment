/**
 * Copyright 2016 Daniel Geerts <daniel.geertsu@live.nl>
 *
 * @file Explosion.h
 */

#ifndef EXPLOSION_H
#define EXPLOSION_H

 // Include NHTVgame files
#include "basicentity.h"
#include "particlesystem.h"

/// @brief The Explosion class is the Entity implementation.
class Explosion : public BasicEntity
{
public:
	/// @brief Constructor
	Explosion(bool isBomb);
	/// @brief Destructor
	virtual ~Explosion();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	ParticleSystem* particlesystem;

private:
	/* add your private declarations */

};

#endif /* EXPLOSION_H */
