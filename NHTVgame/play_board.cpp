/**
 * This class describes Play_Board behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "Play_Board.h"

Play_Board::Play_Board(int scale, Input* Scene_Input) : Entity()
{
	// Define Scale, posX, posY
	Scale = scale;
	int posX = scale / 2;
	int posY = scale * 1.5;

	scene_input = Scene_Input;

	// Set box item position standard to -1 (meaning mouse is not hovering above the play_board)
	mouseHoverBoardItemPosition = -1;

	// Setting child hori_left_line
	hori_left_line = new Line_Entity();
	hori_left_line->_line->addPoint(-posX, -posY);
	hori_left_line->_line->addPoint(-posX, posY);
	hori_left_line->BindPoints(NHTVcolorDARK, lineWidth);

	// Setting child hori_right_line
	hori_right_line = new Line_Entity();
	hori_right_line->_line->addPoint(posX, -posY);
	hori_right_line->_line->addPoint(posX, posY);
	hori_right_line->BindPoints(NHTVcolorDARK, lineWidth);

	// Adding all horizontal childs to the stage
	this->addChild(hori_left_line);
	this->addChild(hori_right_line);

	// Setting child verti_up_line
	verti_up_line = new Line_Entity();
	verti_up_line->_line->addPoint(-posY, -posX);
	verti_up_line->_line->addPoint(posY, -posX);
	verti_up_line->BindPoints(NHTVcolorDARK, lineWidth);

	// Setting child verti_down_line
	verti_down_line = new Line_Entity();
	verti_down_line->_line->addPoint(posY, posX);
	verti_down_line->_line->addPoint(-posY, posX);
	verti_down_line->BindPoints(NHTVcolorDARK, lineWidth);

	// Adding all vertical childs to the stage
	this->addChild(verti_up_line);
	this->addChild(verti_down_line);

	int tempscale = scale * 0.8;

	// Filling Board_Items_Positions
	Board_Items_Positions.push_back(Point2(-tempscale, -tempscale));
	Board_Items_Positions.push_back(Point2(0, -tempscale));
	Board_Items_Positions.push_back(Point2(tempscale, -tempscale));
	Board_Items_Positions.push_back(Point2(-tempscale, 0));
	Board_Items_Positions.push_back(Point2(0, 0));
	Board_Items_Positions.push_back(Point2(tempscale, 0));
	Board_Items_Positions.push_back(Point2(-tempscale, tempscale));
	Board_Items_Positions.push_back(Point2(0, tempscale));
	Board_Items_Positions.push_back(Point2(tempscale, tempscale));

	// filling Board_Items_Placed
	Board_Items_Placed = {
		{ &Board_Items_Positions[0], false },
		{ &Board_Items_Positions[1], false },
		{ &Board_Items_Positions[2], false },
		{ &Board_Items_Positions[3], false },
		{ &Board_Items_Positions[4], false },
		{ &Board_Items_Positions[5], false },
		{ &Board_Items_Positions[6], false },
		{ &Board_Items_Positions[7], false },
		{ &Board_Items_Positions[8], false }
	};
}

Play_Board::~Play_Board()
{
	// deconstruct and delete hori_left_line
	this->removeChild(hori_left_line);
	delete hori_left_line;
	hori_left_line = NULL;

	// deconstruct and delete hori_right_line
	this->removeChild(hori_right_line);
	delete hori_right_line;
	hori_right_line = NULL;

	// deconstruct and delete verti_up_line
	this->removeChild(verti_up_line);
	delete verti_up_line;
	verti_up_line = NULL;

	// deconstruct and delete verti_down_line
	this->removeChild(verti_down_line);
	delete verti_down_line;
	verti_down_line = NULL;
}

void Play_Board::update(float deltaTime)
{
	// ###############################################################
	// Loop trough Board_Items_Positions to check if the mouse
	// is hovering above one of the boxes
	// ###############################################################
	for (int i = 0; i < Board_Items_Positions.size(); i++) {
		if ((scene_input->getMouseX() <= this->position.x + Board_Items_Positions[i].x + (Scale / 2.5)) &&
			(scene_input->getMouseX() >= this->position.x + Board_Items_Positions[i].x - (Scale / 2.5)) &&
			(scene_input->getMouseY() <= this->position.y + Board_Items_Positions[i].y + (Scale / 2.5)) &&
			(scene_input->getMouseY() >= this->position.y + Board_Items_Positions[i].y - (Scale / 2.5))) {
			mouseHoverBoardItemPosition = i;
			break;
		} else {
			// if not hovering above play_board then return -1
			mouseHoverBoardItemPosition = -1;
		}
	}
}

// Return mouse hover above Board Item Position
// When return -1, mouse is not hovering above this play_board
int Play_Board::GetMouseHoverBoardItemPosition() {
	return mouseHoverBoardItemPosition;
}