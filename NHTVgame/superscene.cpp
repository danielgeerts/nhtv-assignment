/**
 * This class describes SuperScene behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include <fstream>
#include <sstream>

#include "SuperScene.h"

int SuperScene::activescene = -1;

SuperScene::SuperScene() : Scene()
{
	// Start FPS timer
	fpstimer.start();

	// set showMousePosition standard to false
	showMousePosition = false;

	// Add 16 lines to the Global text
	// Each line has a different position
	for (unsigned int i = 0; i < 16; i++) {
		Text* line = new Text();
		line->position = Point2(20, 20 + (30 * i));
		line->scale = Point2(0.5f, 0.5f);

		text.push_back(line);
		this->addChild(line);
	}

	// Load the first scene to 0, so when you set a custom scene its 'activescene' to 0, that scene will load
	activescene = 0;
}


SuperScene::~SuperScene()
{
	// deconstruct and delete elements from text
	for (int i = 0; i < text.size(); i++) {
		this->removeChild(text[i]);
		delete text[i];
		text[i] = NULL;
	}
}

void SuperScene::update(float deltaTime)
{
	// ###############################################################
	// Escape key stops the Scene
	// ###############################################################
	if (input()->getKeyUp( GLFW_KEY_ESCAPE )) {
		this->stop();
		activescene = 0;
	}

	// ###############################################################
	// show FPS
	// ###############################################################
	static int framecounter = 0;
	if (fpstimer.seconds() > 1.0f) {
		std::string fpstxt = "FPS: ";
		fpstxt.append(rt2d::to_string<int>(framecounter));
		text[2]->message(fpstxt);
		framecounter = 0;
		fpstimer.start();
	}
	framecounter++;

	// ###############################################################
	// show Mouse Position
	// ###############################################################
	if (showMousePosition) {
		text[3]->message("MousePos, X: " + std::to_string(input()->getMouseX()) + ", Y:" + std::to_string(input()->getMouseY()));
	}
}
