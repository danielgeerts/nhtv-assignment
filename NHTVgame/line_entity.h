/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Line_Entity.h
 */

#ifndef LINE_ENTITY_H
#define LINE_ENTITY_H

#include <rt2d/entity.h>

/// @brief The Line_Entity class is the Entity implementation.
class Line_Entity : public Entity
{
public:
	/// @brief Constructor
	Line_Entity();
	/// @brief Destructor
	virtual ~Line_Entity();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief Local Line
	Line* _line;

	/// @brief Bind all points, with a color and linewidth
	void BindPoints(RGBAColor color, float linewidth);
};

#endif /* LINE_ENTITY_H */
