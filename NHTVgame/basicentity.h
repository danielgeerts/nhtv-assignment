/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file BasicEntity.h
 */

#ifndef BASICENTITY_H
#define BASICENTITY_H

#include <rt2d/entity.h>

/// @brief The BasicEntity class is the Entity implementation.
class BasicEntity : public Entity
{
public:
	/// @brief Constructor
	BasicEntity();
	/// @brief Destructor
	virtual ~BasicEntity();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief Boolean 'trigger' can be used in extended files from BasicEntity files for a trigger
	/// For example: You can acces this trigger from a array with the type BasicEntity and use
	/// this trigger in the extended files, after use set this to false
	bool trigger = false;
};

#endif /* BASICENTITY_H */
