/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Scene_Level_vs_Player.h
 */

#ifndef SCENE_LEVEL_VS_PLAYER_H
#define SCENE_LEVEL_VS_PLAYER_H

// Include NHTVgame files
#include "scene_level.h"
#include "player.h"

/// @brief The Scene_Level_vs_Player class is the Scene implementation.
class Scene_Level_vs_Player : public Scene_Level
{
public:
	/// @brief Constructor
	Scene_Level_vs_Player();
	/// @brief Destructor
	virtual ~Scene_Level_vs_Player();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief restart level when 'play_again' is clicked
	virtual void restartLevel();

private:
	/// @brief child Timer t
	Timer t;

	/// @brief amout of bombWins wins
	int bombWins;
	/// @brief amout of grenadeWins wins
	int grenadeWins;
	/// @brief amout of nobodyWins wins
	int nobodyWins;
};

#endif /* SCENE_LEVEL_VS_PLAYER_H */
