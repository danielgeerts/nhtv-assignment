/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file SuperScene.h
 */

#ifndef SUPERSCENE_H
#define SUPERSCENE_H

#include <rt2d/scene.h>
#include <rt2d/text.h>

/// @brief The SuperScene class is the Scene implementation.
class SuperScene : public Scene
{
public:
	/// @brief Constructor
	SuperScene();
	/// @brief Destructor
	virtual ~SuperScene();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief current activescene, if changes then load another Scene in Main.cpp
	static int activescene;
	/// @brief when true, show mouse position
	bool showMousePosition;

protected:
	/// @brief array with standard Scene Text, easy to use in extended SuperScene files
	std::vector<Text*> text;

private:
	/// @brief FPS (frames per second) timer
	Timer fpstimer;
};

#endif /* SUPERSCENE_H */
