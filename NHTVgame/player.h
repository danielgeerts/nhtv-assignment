/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Player.h
 */

#ifndef PLAYER_H
#define PLAYER_H

#include <rt2d/entity.h>

 // Include NHTVgame files
#include "bomb.h"
#include "grenade.h"

 /// @brief all items to place
enum Items { BOMB, GRENADE, NONE };

/// @brief The Player class is the Entity implementation.
class Player : public Entity
{
public:
	/// @brief current item using
	Items currenItem;

	/// @brief Constructor
	Player(Items currenItem);
	/// @brief Destructor
	virtual ~Player();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

	/// @brief Change the current holding item
	void ChangeHoldingItem(Items CurrentItem);

private:
	/// @brief current items holding
	Entity* currentItemHolding;
};

#endif /* PLAYER_H */
