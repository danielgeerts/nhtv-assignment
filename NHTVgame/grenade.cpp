/**
 * This class describes Grenade behavior.
 *
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 */

#include "Grenade.h"

Grenade::Grenade() : BasicEntity()
{
	// Setting sprite grenade
	grenade = new BasicEntity();
	grenade->addSprite("assets/grenade.tga");
	grenade->scale = Point2(0.75f, 0.75f);

	// Add sprite grenade to this BasicEntity
	this->addChild(grenade);

	// Setting spritesheet explosion
	explosion = new Explosion(false);
}

Grenade::~Grenade()
{
	// deconstruct and delete explosion
	if (explosion != NULL) {
		grenade->removeChild(explosion);
		delete explosion;
		explosion = NULL;
	}

	// deconstruct and delete grenade
	if (grenade != NULL) {
		this->removeChild(grenade);
		delete grenade;
		grenade = NULL;
	}
}

void Grenade::update(float deltaTime)
{
	// ###############################################################
	// trigger from BasicEntity
	// ###############################################################
	if (trigger) {
		// Add the explosion to the bomb, so this program will draw the exlosion
		if (explosion != NULL && addExplosion) {
			explosion->trigger = trigger;
			grenade->addChild(explosion);
			addExplosion = false;
		}

		// Remove grenade sprite when trigger is triggered
		if (explosion->particlesystem->trigger) {
			grenade->deleteSprite();
		}

		// Remove everything when the bomb is exploded
		if (explosion->particlesystem->particles.size() == 0 && !addExplosion) {
			if (explosion != NULL) {
				grenade->removeChild(explosion);
				delete explosion;
				explosion = NULL;
			}
			if (grenade != NULL) {
				this->removeChild(grenade);
				delete grenade;
				grenade = NULL;
			}
			trigger = false;
		}
	}
}
