/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @file Scene_Menu.h
 */

#ifndef SCENE_MENU_H
#define SCENE_MENU_H

#include <rt2d/scene.h>

// Include NHTVgame files
#include "superscene.h"
#include "button.h"

/// @brief The Scene_Menu class is the Scene implementation.
class Scene_Menu : public SuperScene
{
public:
	/// @brief Constructor
	Scene_Menu();
	/// @brief Destructor
	virtual ~Scene_Menu();

	/// @brief update is automatically called every frame
	/// @param deltaTime the elapsed time in seconds
	/// @return void
	virtual void update(float deltaTime);

private:
	/// @brief child Level_1v1
	Button* Level_1v1;
	/// @brief child Level_1vCom
	Button* Level_1vCom;
	/// @brief child title
	Text* title;
	/// @brief child credits
	std::vector<Text*> credits;
};

#endif /* SCENE_MENU_H */
