/**
 * Copyright 2016 Daniel Geerts <daniel.geerts@live.nl>
 *
 * @brief Description of My Awesome Game.
 *
 * @file main.cpp
 *
 * @mainpage Tic Tac Boom
 *
 * @section intro Introduction
 */
#include <rt2d/core.h>

#include "scene_level.h"
#include "scene_level_vs_player.h"
#include "scene_level_vs_pc.h"
#include "scene_menu.h"

/// @brief main entry point
int main( void )
{
	// Core instance
	Core core;

	// Scene_Level
	Scene_Menu* scene_menu = new Scene_Menu(); // create Scene on the heap
	Scene_Level* level_vs_player = new Scene_Level_vs_Player(); // create Scene on the heap
	Scene_Level* level_vs_computer = new Scene_Level_vs_PC(); // create Scene on the heap


	// Vector of all scenes
	std::vector<SuperScene*> all_scenes;
	all_scenes.push_back(scene_menu);
	all_scenes.push_back(level_vs_player);
	all_scenes.push_back(level_vs_computer);

	// 'Current scene' counter
	static int scenecounter = 0;
	// If running = 0, application will close
	static int running = 1;

	while(running == 1) { // check running every frame
		// if SuperScene::activescene is changed
		// Load another scene
		if (scenecounter != SuperScene::activescene) {
			all_scenes[scenecounter]->stop(); // Stop current scene
			scenecounter = SuperScene::activescene;	// Change current scene
			all_scenes[scenecounter]->start();	// Start new current scene
			core.run(all_scenes[scenecounter]); // update and render the current scene
			core.showFrameRate(5); // show framerate in output every n seconds

			// Restart the level so you will start with a clean play_board, and the first player
			if (scenecounter == 1) {
				level_vs_player->restartLevel();
			} else if (scenecounter == 2) {
				level_vs_computer->restartLevel();
			}
		}
		else {
			scenecounter = SuperScene::activescene; //  set scenecounter to SuperScene::activescene
			core.run(all_scenes[scenecounter]); // update and render the current scene
			core.showFrameRate(5); // show framerate in output every n seconds
		}

		// if ESCAPE is pressed and current scene is 0, then close the application
		if (glfwGetKey(core.getRenderer()->window(), GLFW_KEY_ESCAPE) == GLFW_PRESS) {
			if (SuperScene::activescene == 0) {
				running = 0;
			}
		}

		// When clicking the red 'X' in the right corner of the application, then close the appliction
		if (glfwWindowShouldClose(core.getRenderer()->window()) != 0) {
			running = 0;
		}
	}
	core.cleanup(); // cleanup ResourceManager (Textures + Meshes, but not Shaders)
	for (int i = 0; i < all_scenes.size(); i++) {
		delete all_scenes[i]; // delete all Scenes and everything in it from the heap
	}

	// No need to explicitly clean up the core.
	// As a local var, core will go out of scope and destroy Renderer->ResourceManager.
	// ResourceManager destructor also deletes Shaders.

	return 0;
}
